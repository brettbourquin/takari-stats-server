/*******************************************************************************
 * Copyright (c) 2013 Takari, Inc.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *      Takari, Inc. - initial API and implementation
 *******************************************************************************/

package io.takari.stats.server;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Jetty handler that records usage stats in slf4j logger. Handles {@code PUT} requests at
 * {@code /stats} path.
 */
public class StatsJettyHandler extends AbstractHandler {

  private static final Logger stats = LoggerFactory.getLogger("STATS");

  private static final SimpleDateFormat DATEFORMAT;

  static {
    DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
    DATEFORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
  }

  public void handle(String target, Request baseRequest, HttpServletRequest request,
      HttpServletResponse response) throws IOException, ServletException {
    if ("/stats".equals(target) && "PUT".equals(baseRequest.getMethod())) {
      StringBuilder sb = new StringBuilder();
      append(sb, DATEFORMAT.format(new Date()));
      append(sb, hash(getClientIp(request)));
      append(sb, getBody(request));
      stats.info(sb.toString());
      baseRequest.setHandled(true);
    }
  }

  private String getBody(HttpServletRequest request) throws IOException {
    char[] body = new char[50 * 1024]; // limit accepted body size
    int len = request.getReader().read(body);
    return len > 0 ? new String(body, 0, len) : "";
  }

  private String getClientIp(HttpServletRequest request) {
    String addr = request.getHeader("X-Forwarded-For");
    if (addr == null) {
      addr = request.getRemoteAddr();
    }
    return addr;
  }

  private void append(StringBuilder sb, String str) {
    if (sb.length() > 0) {
      sb.append('\t');
    }
    if (str != null && !str.isEmpty()) {
      sb.append(str);
    }
  }

  private String hash(String str) throws IOException {
    try {
      MessageDigest digest = MessageDigest.getInstance("MD5");
      if (str != null) {
        digest.update(str.getBytes("UTF8"));
      }
      return unpaddedBase64(digest.digest());
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

  private static final byte[] MAP = new byte[] {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
      'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b',
      'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
      'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

  private String unpaddedBase64(byte[] in) throws IOException {
    int length = (in.length + 2) * 4 / 3;
    byte[] out = new byte[length];
    int index = 0, end = in.length - in.length % 3;
    for (int i = 0; i < end; i += 3) {
      out[index++] = MAP[(in[i] & 0xff) >> 2];
      out[index++] = MAP[((in[i] & 0x03) << 4) | ((in[i + 1] & 0xff) >> 4)];
      out[index++] = MAP[((in[i + 1] & 0x0f) << 2) | ((in[i + 2] & 0xff) >> 6)];
      out[index++] = MAP[(in[i + 2] & 0x3f)];
    }
    switch (in.length % 3) {
      case 1:
        out[index++] = MAP[(in[end] & 0xff) >> 2];
        out[index++] = MAP[(in[end] & 0x03) << 4];
        // out[index++] = '=';
        // out[index++] = '=';
        break;
      case 2:
        out[index++] = MAP[(in[end] & 0xff) >> 2];
        out[index++] = MAP[((in[end] & 0x03) << 4) | ((in[end + 1] & 0xff) >> 4)];
        out[index++] = MAP[((in[end + 1] & 0x0f) << 2)];
        // out[index++] = '=';
        break;
    }
    return new String(out, 0, index, "US-ASCII");
  }
}
