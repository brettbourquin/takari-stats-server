/*******************************************************************************
 * Copyright (c) 2014 Takari, Inc.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *      Takari, Inc. - initial API and implementation
 *******************************************************************************/

package io.takari.stats.server;

import java.net.InetSocketAddress;
import java.util.StringTokenizer;

import org.eclipse.jetty.server.Server;

/**
 * Takari usages statistics collection server main class. Starts jetty embedded server on specified
 * port.
 * 
 * @see StatsJettyHandler
 */
public class StatsServerMain {
  public static void main(String[] args) throws Exception {
    if (args.length != 1) {
      System.out.print("Usage: StatsServerMain [hostname:]port");
      System.exit(-1);
    }

    InetSocketAddress addr;
    if (args[0].contains(":")) {
      StringTokenizer st = new StringTokenizer(args[0], ":");
      String hostname = st.nextToken();
      int port = Integer.parseInt(st.nextToken());
      addr = new InetSocketAddress(hostname, port);
    } else {
      addr = new InetSocketAddress(Integer.parseInt(args[0]));
    }

    Server server = new Server(addr);
    server.setHandler(new StatsJettyHandler());
    server.start();
    server.join();
  }
}
