package adhoc;

import io.takari.aether.client.AetherClientConfig;
import io.takari.aether.client.Response;
import io.takari.aether.client.RetryableSource;
import io.takari.aether.okhttp.OkHttpAetherClient;

import java.io.IOException;
import java.io.OutputStream;

public class AdhocClientMain {
  public static void main(String[] args) throws IOException {
    AetherClientConfig config = new AetherClientConfig();
    config.setUserAgent("adhoc");
    config.setConnectionTimeout(15 * 1000); // XXX
    config.setRequestTimeout(60 * 1000); // XXX

    final byte[] bytes = "test".getBytes("UTF-8");
    Response response =
        new OkHttpAetherClient(config).put("http://localhost:8443/stats", new RetryableSource() {
          @Override
          public long length() {
            return bytes.length;
          }

          @Override
          public void copyTo(OutputStream os) throws IOException {
            os.write(bytes);
          }
        });
    if (response.getStatusCode() > 399) {
      System.err.println(response.getStatusCode() + "/" + response.getStatusMessage());
    }
  }
}
